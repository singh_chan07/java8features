package lambdas;

import sample.Person;
import sample.Sample;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class LambdaDemo2 {

    public static void main(String[] args) throws IOException {


        List<Person> personList = Sample.loadPersonData();



        /*
        If you want to filter the persons which are from New Orleans city then the filter
        method can be used as like this.
         */
        List<Person> personFromNewOrleans = filter(personList, (Person p)->p.getCity().equals("New Orleans"));

        for(Person p: personFromNewOrleans){
            System.out.println(p.getFirstName() + " " + p.getLastName() + " | " + p.getAddress());
        }


        /*
        If you want to get information about which company people of State of LA work
         */
        List<Person> personsFromLA = filter(personList, p-> p.getState().equals("LA"));

        System.out.println("\n\n");
        for(Person p: personsFromLA){
            System.out.println(p.getFirstName() + " " + p.getLastName() + " works at -> " + p.getCompanyName());
        }


        /*
        Suppose we want to send emergency messages to each person from lives in new orleans
         */
        System.out.println("\n\n"
        );
        process(personList, e->e.getCity().equals("New Orleans"), e->{
            System.out.println("sent emergency message to : " + e.getPhone1() + ", " + e.getPhone2());
        });



    }

    public static <T> void process(Iterable<T> iterable, Predicate<T> predicate, Consumer<T> consumer){
        for(T t: iterable){
            if(predicate.test(t)){
                consumer.accept(t);
            }
        }
    }


    public static<T> List<T> filter(List<T> list, Predicate<T> predicate){
        List<T> resList = new ArrayList<>();
        for(T e : list){
            if(predicate.test(e)){
                resList.add(e);
            }
        }
        return resList;
    }
}
