package lambdas;

import sample.Employee;
import sample.Sample;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/*
This demo Explains various use of the lambda expression. Lambda expression make code more clean.
Allows us to pass methods as arguments, use code as data.
Here are some demo examples of using lambdas
 */


public class LambdasDemo {


    public static void main(String[] args) {

        List<Employee> list = Sample.getSampleData();



        /*
        * Suppose admin wants to encourage employees who have worked more than 24 days and giving special bonuses to them then
        */

        process(list,
                e -> e.getWorkingDays()>24,
                e -> System.out.println(e.getSalary() * (e.getWorkingDays()%10))
        );

        /*
        To calculate Monthly Salary, if the salary given is daily salary
         */

        process(list,
                e -> true,
                e -> System.out.println(e.getWorkingDays()*(e.getSalary()-(e.getSalary()/(e.getGrade()*10))))
        );


        /*
        Suppose that admin may wants to inform all employees of the delivery department for a meet on the mail
         */
        process(list,
                e -> e.getDepartment().equals("Delivery"),
                e-> System.out.println("Mail sent to : " + e.getFirstName() +" "+ e.getLastName())
        );

        /* to deduct taxes from employee salary */
        process( list,
                e -> true,
                e ->{
                    int g = e.getGrade();
                    double s = e.getSalary();
                    System.out.println(s/(g*10)*(e.getWorkingDays()));
                }
        );


        // use of method reference.
        sortByAge(list);



        for(Employee e : list){
            System.out.println(e.toString());
        }


    }


    /*
    In this method, Lambda expressions are used to pass methods as arguments to another method.
    This is one of the best use case of Lambda expression.
    Here a same function/method is used to perform several task by passing different functionalities as arguments.
    And the method process is used to perform various operations on those data.
     */
    public static <T> void process(Iterable<T> list, Predicate<T> pre, Consumer<T> cons){
        for(T element: list){
            if(pre.test(element)){
                cons.accept(element);
            }
        }
    }

    /*
    Another feature of the lambda expression is method reference which let us use any method of an object
    to be used as an implementation of the functional interface. The method that can be used as a method
    reference it must have same method signature as that of the method of the functional interface.
     */
    /*
    In this particular case Comparator is a functional interface and compareByAge is static method defined in employees class
     */
    public static void sortByAge(List<Employee> list){
        Collections.sort(list, Employee::compareByAge);
    }




}
