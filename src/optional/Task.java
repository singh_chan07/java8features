package optional;

import java.util.Optional;

public class Task {
    private final String title;
    private final User assignedTo;
    private final String id;

    public Task(String id, String title) {
        this.id = id;
        this.title = title;
        this.assignedTo = null;
    }

    public Task(String id, String title, User assignedTo) {
        this.id = id;
        this.title = title;
        this.assignedTo = assignedTo;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Optional<User> getAssignedTo() {
        return Optional.of(assignedTo);
    }
}
