package optional;

public class Address {
    private String street;
    private int houseNumber;
    private String landMark;
    private String district;
    private String state;
    private String country;
    private int zipCode;

    public Address(String street, int houseNumber, String landMark, String district, String state, String country, int zipCode) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.landMark = landMark;
        this.district = district;
        this.state = state;
        this.country = country;
        this.zipCode = zipCode;
    }

    public String getStreet() {
        return street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public String getLandMark() {
        return landMark;
    }

    public String getDistrict() {
        return district;
    }

    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    public int getZipCode() {
        return zipCode;
    }
}
