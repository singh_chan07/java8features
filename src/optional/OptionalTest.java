package optional;

public class OptionalTest {


    private static TaskRepository repository = new TaskRepository();
    public static void main(String[] args) throws TaskNotFoundException {
        Task t1 = new Task("121", "Create a Repo");
        Task t2 = new Task("1211", "List Java 8 features", new User("Chandra Singh"));
        Task t3 = new Task("1231", "Fill timesheet", new User("Chandra Singh", "41, OmVihar, Ajabpur"));
        Task t4 = new Task("1211", "Enroll for benifit me", new User("Chandra Singh", "41, OmVihar"));


        repository.add(t1);
        repository.add(t2);
        repository.add(t3);
        repository.add(t4);


        System.out.println(taskTitle("1231"));

        System.out.println(taskAssignedTo("1231"));


    }



    public static String taskTitle(String taskId) throws TaskNotFoundException {
        return repository.
                find(taskId).
                map(Task::getTitle).
                orElseThrow(() -> new TaskNotFoundException(String.format("No task exist for id '%s'",taskId)));
    }


    public static String taskAssignedTo(String taskId) {
        return repository.
                find(taskId).
                flatMap(task -> task.getAssignedTo().map(user -> user.getUsername())).
                orElse("NotAssigned");
    }



}
