package optional;

import sample.Employee;

import java.io.IOException;
import java.util.Optional;

public class OptionalDemo {
    public static void main(String[] args) throws IOException {

        Optional<Employee> employeeOptional = Optional.empty();

        System.out.println(employeeOptional.isPresent());




        Employee employee = new Employee(1532, "Ravish", "Kumar", 43, 7, "Recruitment", 5000.0, 28);

        employeeOptional = Optional.of(employee);
        System.out.println(employeeOptional.isPresent());



        employeeOptional.ifPresent(e->{
            System.out.println(e);
        });




        Person person = new Person("Ram", "Sharma", new Address("Johans street", 41, "Opposite to Carara Bank", "New Orleans", "LA", "US", 543212), "csbisht926@gmail.com");


    }
}
