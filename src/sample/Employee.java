package sample;

public class Employee {

    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private int grade;
    private String department;
    private double salary;
    private int workingDays;



    public Employee(int id, String firstName, String lastName, int age, int grade, String department, double salary, int workingDays) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.grade = grade;
        this.department = department;
        this.salary = salary;
        this.workingDays = workingDays;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public int getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(int workingDays) {
        this.workingDays = workingDays;
    }


    @Override
    public String toString() {
        return "first name : " + firstName + " last name : " + lastName + " age : " + age + " salary  : "+ salary + " department : " + department;
    }

    public static int compareByAge(Employee e1, Employee e2){
        if(e1.getSalary()>e2.getSalary()){
            return 1;
        }else if(e1.getSalary()<e2.getSalary()){
            return -1;
        }else{
            return 0;
        }
    }
}
