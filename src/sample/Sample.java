package sample;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Sample {


    public static List<Employee>  getSampleData(){
        Employee [] employees ={
                new Employee(121, "Ram", "Verma", 40, 5, "Delivery", 40000, 15),
                new Employee(122, "Krishna", "Verma", 26, 2, "Delivery", 5000, 20),
                new Employee(123, "Aishwarya", "Verma", 24, 1, "Delivery", 3500, 24),
                new Employee(124, "Ramesh", "Verma", 24, 1, "Delivery", 3500, 20),
                new Employee(125, "Komal", "Verma", 24, 1, "Delivery", 3500, 21),
                new Employee(126, "Ram", "Verma", 24, 1, "Recruitment", 3500, 16),
                new Employee(127, "Suresh", "Verma", 35, 4, "Delivery", 35000, 16),
                new Employee(128, "Sudesh", "Verma", 24, 1, "Sales", 3500, 24),
                new Employee(129, "Himesh", "Verma", 40, 6, "Delivery", 40000, 24),
                new Employee(130, "Paul", "Verma", 24, 1, "Delivery", 3500, 24),
                new Employee(131, "Lokesh", "Verma", 32, 3, "HR", 30000, 21),
                new Employee(132, "Kamlesh", "Verma", 26, 2, "Delivery", 5000, 21),
                new Employee(133, "Girish", "Verma", 24, 1, "HR", 3500, 24),
                new Employee(134, "Jack", "Verma", 32, 3, "Delivery", 30000, 21),
                new Employee(135, "Suchita", "Verma", 24, 1, "Delivery", 3500, 20),
                new Employee(136, "Yash", "Verma", 32, 3, "Delivery", 30000, 21),
                new Employee(137, "Rohit", "Verma", 24, 1, "Recruitment", 3500, 25),
                new Employee(138, "Tanuja", "Verma", 24, 1, "Delivery", 3500, 26),
                new Employee(139, "Manmohan", "Verma", 35, 4, "Delivery", 35000, 24),
                new Employee(140, "Somesh", "Verma", 24, 1, "HR", 3500, 27)
        };
        List<Employee> list = Arrays.asList(employees);
        return list;
    }

    public static List<Person> loadPersonData() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(".\\.\\sample\\us-500.csv"));
        br.readLine();

        String line;
        List<Person> personList = new ArrayList<>();
        while((line=br.readLine())!=null){
            String [] strings = line.split("\\|");
            Person person = new Person(strings[0],strings[1],strings[2],strings[3],strings[4],strings[5],strings[6],Integer.valueOf(strings[7]),strings[8],strings[9],strings[10],strings[11]);
            personList.add(person);
        }
        return personList;
    }
}
