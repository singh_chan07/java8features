import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class Filter {

    static class Item{
        String name;
        double price;
        String category;
        String brand;
        static int inStock;


    }


    public static void main(String [] args){



    }


    public static<T> List<T> filter(Iterable<T> list, Predicate<T> tester, Function<T, T> mapper){
        List<T> iList = new ArrayList<>();
        for(T i: list){
            if(tester.test(i)){
                iList.add(mapper.apply(i));
            }
        }
        return iList;
    }
}
