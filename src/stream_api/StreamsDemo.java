package stream_api;

import sample.Employee;
import sample.Sample;

import java.util.List;
import java.util.stream.Collectors;

public class StreamsDemo {

    public static void main(String[] args) {

        //sample data
        List<Employee> list = Sample.getSampleData();

        System.out.println(totalCTC(list));

        list = sortByAge(list);

        for(Employee e: list){
            System.out.println(e);
        }


        System.out.println("\n\n");
        list = sortByName(list);




        for(Employee e: list){
            System.out.println(e);
        }


    }


    /*
    This method makes use of streams to sort employees by their age.
     */
    public static List<Employee> sortByAge(List<Employee> list){
        return list.stream().sorted((e1, e2)->e1.getAge()-e2.getAge()<0?-1:1).collect(Collectors.toList());
    }

    /*
    THis sorts the list by name using
     */
    public static List<Employee> sortByName(List<Employee> list){
        return list.stream().sorted((e1, e2) ->{
            if(e1.getFirstName().equals(e2.getFirstName())){
                if(e1.getLastName().equals(e2.getLastName())){
                    return 0;
                }else if(e1.getLastName().compareTo(e2.getLastName())<0){
                    return -1;
                }else{
                    return 1;
                }
            }else{
                return e1.getFirstName().compareTo(e2.getFirstName())<0?-1: 1;
            }
        }).collect(Collectors.toList());
    }


    /*
    Calculates total CTC per month
     */
    public static double totalCTC(List<Employee> list){
        return list.stream().map(e->e.getSalary()*30).reduce(0d, (e1, e2)->e1+e2);
    }



}
