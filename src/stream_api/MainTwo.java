package stream_api;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MainTwo {


    /*
    Filtering of the streams
     */

    public static void main(String[] args) {


        Integer [] arr = {12, 23, 121, 121, 34, 54, 34, 54, 67, 89, 23, 56, 34, 12, 99};
        List<Integer> list = Arrays.asList(arr);

        //Filtering with a predicate
        List<Integer> evens = list.stream().filter(e->e%2==0).collect(Collectors.toList());
        printList(evens);


        //Filtering unique elements in stream
        System.out.println("total items in the list : " + list.size());
        List<Integer> distinct = list.stream().distinct().collect(Collectors.toList());
        printList(distinct);
        System.out.println("Distinct items in the list : " + distinct.size());



        //truncating the stream
        List<Integer> tList = list.stream().limit(6).collect(Collectors.toList());
        printList(tList);

        //skipping elements
        List<Integer> skippedList = list.stream().skip(5).collect(Collectors.toList());
        printList(skippedList);



        String s = "THis is some random text i am throwing for the purpose of understanding how flatmap works";
        String [] sarr = s.split(" ");
        List<String> sList = Arrays.asList(sarr);

        List<String> uniqueChars = sList.stream().map(w ->w.split("")).flatMap(Arrays::stream).distinct().collect(Collectors.toList());

        printList(uniqueChars);


        //Finding an element
        Optional<Character> item = uniqueChars.stream().map(w->w.charAt(0)).filter(c->c>97&&c<110).findAny();

        item.ifPresent(c-> System.out.println(c));



        //Maximum & Minimum
        Integer max = list.stream().max(Comparator.comparing(e->e)).get();
        System.out.println(max);

        Integer min = list.stream().min(Comparator.comparing(e->e)).get();
        System.out.println(min);
    }

    public static<T> void printList(List<T> list){
        for(T i: list){
            System.out.print(i+" ");
        }
        System.out.println();
    }
}
