package stream_api;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.Stream;

public class StreamMain {

    public static void main(String[] args) {


        //Creating streams from values
        Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5, 6, 7);



        //Creating streams from Arrays
        Integer [] arr = {12, 34, 232, 232, 2, 1, 5};
        Stream<Integer> stream1 = Stream.of(arr);

        //Creating stream from Files
        try{
            BufferedReader br = new BufferedReader(new FileReader(".\\.\\sample\\us-500.csv"));

            Stream<String> stringStream = Files.lines(Paths.get(".\\.\\sample\\us-500.csv"));
            long count = stringStream.map(w->w.split(" ")).flatMap(Arrays::stream).count();
            System.out.println(count);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        //generating stream

        Stream<Integer> integerStream = Stream.generate(()->new Random().nextInt());
        integerStream.limit(20).forEach(c-> System.out.println(c));
    }
}
