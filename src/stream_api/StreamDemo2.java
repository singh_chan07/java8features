package stream_api;

import java.util.Random;
import java.util.stream.Stream;

public class StreamDemo2 {

    public static void main(String[] args) {

        //Here Stream.generate method uses a supplier to produce a stream of doubles
        //This is one of the way to create stream.
        Stream<Double> doubleStream = Stream.generate(()->new Random().nextDouble());


        //This will create a stream of 20 elements which will get printed and get the sum is get printed;
        double sum = doubleStream.mapToDouble(e -> {
            System.out.println(e);
            return e;
        }).limit(20).sum();
        System.out.println(sum);
    }
}
