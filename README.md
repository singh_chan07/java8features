Java 8 Features
===============
Most Important features of Java 8 are -
1. Lambda Expressions, Method references, Functional Interfaces.
2. Streams, Collector interface, Collectors.
3. Default methods, Static methods
4. Optional Class

Lambda Expressions, Method references, Functional Interfaces
-----------------------------------------------------------
**Lambda Expression** - 
A lambda expression is a syntactical structure in java that allows us to write clean code, pass method as argument or treat code as data.
Lambdas allow us to write code which don't need a  function name, paramenters to be described as long as the relevant functional interface
exists.

**Functional Interface** -
Functional interface is an interface which have only and only one abstract method, it can have multiple default methods.

There are several functional interfaces which java provides us to work with few of them are as follws:
1. **Supplier**: Supplier functional interface is parameter less which is use to provide some value on its own.
2. **Function**: Function is functional interface which takes a paramenter and returns a result
3. **BiFunction**: BiFunction takes two arguments and process those and returns a result.
4. **BinaryOperator**: BinaryOperator takes two arguments of same type and returns a result of the same type as of arguments.
5. **Predicate**: Predicate checks whether a condition is true or not for the arguement.
6. **Consumer**: Consumer takes an argument and returns no result.

Java provide these functional interfaces to work with lambdas.

**Method Reference** - 
It another feature in Java 8 that allows any method, whether static, instance to be passed in place of Lambdas as long as the function descriptor and method signature matches.

Stream API, Collectors class, Collector Interface
-----------------
**Streams** : Stream is basically an abstraction of sequence of data elements from a Collection/data source, which gives us the capability to
perform various operations on that data without affecting the actual datasource/collections. It acts as a pipeline.
Streams support intermediate and terminal operations. Intermediate operations do not modify the acual stream instead they create
new streams and we perform multiple intermediate operations such as filter, map and in the end we can perform terminal operation
which reduces the data to an accumulator.

1. Streams are abstraction which basically represents flow of elements from collection. 
2. Streams give us capabilities to perform various operations declaratively with the help of lambda expressions.
3. It allows us to perform operations in lazy loaded manner which means only those operations are performed which are reruired by the terminal operation.


**Collectors** : Collectors is analogous to Collections class. As the Collections class contains
utility methods to work with difference collection implementations, similarly the Collectors class allows 
us to work with Streams.

Optional Class
---------------
Java 8 provides an Optional Class to work in the situations where 
null might exist. This class wraps a class and allows programmers
to work when an unintentional null might occur.
It contains methods such as ifPresent, isPresent, get() to work with
the class it encapsulates.



Default Methods
------------------

With java 8, Interfaces can provide method with implementations
and these are called default methods, After its inclusion programmers
now can either provide implementation to a method or just don't implement it.
